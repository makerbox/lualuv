player     = {}
fish       = {}
cwide      = 520
chigh      = 333

love.window.setMode(cwide, chigh)
	
function love.load()
    fish.x    = 0
    fish.y    = 0
    fish.img  = love.graphics.newImage( 'images/fish.png' )
    fish.wide = 142
    fish.high = 86

    player.img   = love.graphics.newImage('images/tux.png')
    player.x     = 0
    player.y     = 0
    player.wide  = player.img:getWidth()
    player.high  = player.img:getHeight()
    player.speed = 10
end

function love.update(dt)
    if love.keyboard.isDown("right") then
        if player.x <= (love.graphics.getWidth() - player.img:getWidth()) then
	player.x = player.x+player.speed
	plritate()
    end

    elseif love.keyboard.isDown("left") then
        if player.x <= (love.graphics.getWidth() + player.wide - player.img:getWidth()) then
	player.x = player.x-player.speed
	plrotate()
    end
    elseif love.keyboard.isDown("up")  then
	player.y = player.y-player.speed    

    elseif love.keyboard.isDown("down") then
	player.y = player.y+player.speed
end

    if CheckCollision(fish.x,fish.y,fish.img:getWidth(),fish.img:getHeight(), player.x,player.y,player.img:getWidth(),player.img:getHeight() ) then
        fdead()
    else
	falive()
	
    end

    aimove(fish,1,0,fish.wide,fish.high)
end

function love.draw()
    love.graphics.setColor(233,233,234)
    love.graphics.draw(player.img,player.x,player.y,0,1,1,0, 0)
    love.graphics.draw(fish.img, fish.x, fish.y, 0, 1, 1, 0, 0)    
end

function aimove(obj,x,y,ox,oy)
    if obj.x == 300 then
       edgeleft  = 1
       edgeright = 0
    end

    if obj.x == 0 then
       edgeright = 1
       edgeleft  = 0
    end

    if edgeright == 1 then
        obj.x = obj.x + x
        obj.y = obj.y + y
    else
        obj.x = obj.x - x
        obj.y = obj.y - y
    end
end

function CheckCollision(x1,y1,w1,h1, x2,y2,w2,h2)
  return x1 < x2+w2 and
         x2 < x1+w1 and
         y1 < y2+h2 and
         y2 < y1+h1
end

function plrotate()
    player.img = love.graphics.newImage('images/tux1.png') 
end

function plritate()
    player.img = love.graphics.newImage('images/tux.png' )
end

function falive()
fish.img = love.graphics.newImage('images/fish.png')
end

function fdead()
fish.img = love.graphics.newImage('images/fishbones.png')
end