require("player")

love.window.setTitle(' Tux phys game ')

player   = {}
platform = {}
block    = {}
canw     = 800
canv     = 650

function love.load()
    love.physics.setMeter(64) --1 meter = 64px
    world = love.physics.newWorld(0, 9*64, true) --hgrav=0,vgrav=9

    platform.body    = love.physics.newBody(world, canw/2, canv-50/2)
    platform.shape   = love.physics.newRectangleShape(canw, 50)
    platform.fixture = love.physics.newFixture(platform.body, platform.shape);

    player.img   = love.graphics.newImage('images/tux.png')
    player.body  = love.physics.newBody(world, canw/2, canv/2, "dynamic")
    player.shape = love.physics.newRectangleShape(player.img:getWidth(), player.img:getHeight())
    player.fixture = love.physics.newFixture(player.body, player.shape, 1)
    player.fixture:setRestitution(1)
    player.speed = 200
    player.y_velocity = 0
    player.jump = -300
    player.y = canv-50/2
    player.x = love.graphics.getWidth()  / 2	

    block.body = love.physics.newBody(world, 200, 550, "dynamic")
    block.shape = love.physics.newRectangleShape(0, 0, 50, 100)
    block.fixture = love.physics.newFixture(block.body, block.shape, 5)

    love.graphics.setBackgroundColor(104, 136, 248)
    love.window.setMode(canw, canv)
end


function love.draw()
    -- ground
    love.graphics.setColor(246, 246, 250)
    love.graphics.polygon("fill", platform.body:getWorldPoints(platform.shape:getPoints()))

    -- player
    love.graphics.setColor(255, 255, 255)
    love.graphics.draw(player.img, player.body:getX(), player.body:getY(), player.body:getAngle(),1,1,player.img:getWidth()/2, player.img:getHeight()/2)

    -- block
    love.graphics.setColor(50, 50, 50)
    love.graphics.polygon("fill", block.body:getWorldPoints(block.shape:getPoints()))
    
    love.graphics.setColor(0, 0, 0)
    love.graphics.print(player.body:getY(), canw/2, canv/2)
    love.graphics.print(platform.body:getY(), canw/3, canv/2)
end
